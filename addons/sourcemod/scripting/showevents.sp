#pragma semicolon 1

#include <sourcemod>

#define CONSOLE_PREFIX "[EVENTS] "

new Handle:hGameEvents = INVALID_HANDLE;
new Handle:hModEvents = INVALID_HANDLE;

new Handle:sm_show_events = INVALID_HANDLE;
new bool:bShowEvents = false;

public Plugin:myinfo = {
	name = "[ANY] Show Events",
	author = "Leonardo",
	description = "Prints events to the server console",
	version = "1.1-20160623",
	url = "http://www.xpenia.org"
};

public OnPluginStart()
{
	HookConVarChange( sm_show_events = CreateConVar( "sm_show_events", bShowEvents ? "1" : "0", "Show events in console?", FCVAR_PLUGIN ), OnConVarChanged );
	
	
	new String:szEventName[64];
	
	hGameEvents = CreateKeyValues( "GameEvents" );
	if( FileToKeyValues( hGameEvents, "resource/GameEvents.res" ) )
	{
		PrintToServer( CONSOLE_PREFIX ... "Loading data from GameEvents.res" );
		
		KvRewind( hGameEvents );
		if( KvGotoFirstSubKey( hGameEvents ) )
			do
			{
				KvGetSectionName( hGameEvents, szEventName, sizeof( szEventName ) );
				if( strlen( szEventName ) > 0 )
				{
					PrintToServer( CONSOLE_PREFIX ... "Hooking game event '%s'", szEventName );
					HookEventEx( szEventName, OnGameEvent, EventHookMode_Post );
				}
			}
			while( KvGotoNextKey( hGameEvents ) );
	}
	else
		PrintToServer( CONSOLE_PREFIX ... "Failed to locate GameEvents.res" );
	
	hModEvents = CreateKeyValues( "ModEvents" );
	if( FileToKeyValues( hModEvents, "resource/ModEvents.res" ) )
	{
		PrintToServer( CONSOLE_PREFIX ... "Loading data from ModEvents.res" );
		
		KvRewind( hModEvents );
		if( KvGotoFirstSubKey( hModEvents ) )
			do
			{
				KvGetSectionName( hModEvents, szEventName, sizeof( szEventName ) );
				if( strlen( szEventName ) > 0 )
				{
					PrintToServer( CONSOLE_PREFIX ... "Hooking mod event '%s'", szEventName );
					HookEventEx( szEventName, OnModEvent, EventHookMode_Post );
				}
			}
			while( KvGotoNextKey( hModEvents ) );
	}
	else
		PrintToServer( CONSOLE_PREFIX ... "Failed to locate ModEvents.res" );
}

public OnConfigsExecuted()
	bShowEvents = GetConVarBool( sm_show_events );
public OnConVarChanged( Handle:hConVar, const String:szOldValue[], const String:szNewValue[] )
	OnConfigsExecuted();

public OnGameEvent( Handle:hEvent, const String:szEventName[], bool:bDontBroadcast )
	ShowEventInfo( hEvent, hGameEvents );
public OnModEvent( Handle:hEvent, const String:szEventName[], bool:bDontBroadcast )
	ShowEventInfo( hEvent, hModEvents );

stock ShowEventInfo( &Handle:hEvent, &Handle:hKV )
{
	if( bShowEvents && hKV != INVALID_HANDLE )
	{
		new String:szEventName[128];
		GetEventName( hEvent, szEventName, sizeof( szEventName ) );
		
		KvRewind( hKV );
		if( strlen( szEventName ) > 0 && KvJumpToKey( hKV, szEventName ) )
		{
			new String:szParams[2048];
			new String:szParamName[64];
			new String:szParamType[32];
			new String:szParamValue[256];
			new Handle:hParamNames = CreateArray( RoundToCeil( sizeof( szParamName ) / 4.0 ) );
			
			if( KvGotoFirstSubKey( hKV, false ) )
			{
				do
				{
					KvGetSectionName( hKV, szParamName, sizeof( szParamName ) );
					if( strlen( szParamName ) > 0 )
						PushArrayString( hParamNames, szParamName );
				}
				while( KvGotoNextKey( hKV, false ) );
				KvGoBack( hKV );
				
				for( new i = 0; i < GetArraySize( hParamNames ); i++ )
				{
					GetArrayString( hParamNames, i, szParamName, sizeof( szParamName ) );
					KvGetString( hKV, szParamName, szParamType, sizeof( szParamType ), "" );
					
					szParamValue[0] = '\0';
					
					if( StrEqual( szParamType, "string", false ) )
					{
						GetEventString( hEvent, szParamName, szParamValue, sizeof( szParamValue ) );
						Format( szParamValue, sizeof( szParamValue ), "\"%s\"", szParamValue );
					}
					else if( StrEqual( szParamType, "byte", false ) || StrEqual( szParamType, "int", false ) || StrEqual( szParamType, "long", false ) || StrEqual( szParamType, "short", false ) )
						IntToString( GetEventInt( hEvent, szParamName ), szParamValue, sizeof( szParamValue ) );
					else if( StrEqual( szParamType, "float", false ) )
						FloatToString( GetEventFloat( hEvent, szParamName ), szParamValue, sizeof( szParamValue ) );
					else if( StrEqual( szParamType, "bool", false ) )
					{
						if( GetEventBool( hEvent, szParamName ) )
							strcopy( szParamValue, sizeof( szParamValue ), "true" );
						else
							strcopy( szParamValue, sizeof( szParamValue ), "false" );
					}
					
					if( strlen( szParamName ) > 0 )
						Format( szParams, sizeof( szParams ), "%s%s%s=%s", szParams, ( strlen( szParams ) > 0 ? ", " : "" ), szParamName, szParamValue );
				}
			}
		
			if( strlen( szParamName ) > 0 )
				Format( szParams, sizeof( szParams ), " %s ", szParams );
			
			PrintToServer( CONSOLE_PREFIX ... "%s(%s)", szEventName, szParams );
			for( new i = 1; i <= MaxClients; i++ )
				if( IsClientInGame( i ) && ( GetUserFlagBits( i ) & ADMFLAG_ROOT ) == ADMFLAG_ROOT )
					PrintToServer( "[SM]" ... CONSOLE_PREFIX ... "%s(%s)", szEventName, szParams );
		}
	}
}